/*
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 * SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

document.querySelectorAll('iframe, video')
  .forEach(element => {
    let parent = element.parentNode;
    if (!parent.classList.contains('ratio')) {
      let wrapper = document.createElement('div');

      // set the wrapper as child (instead of the element)
      parent.replaceChild(wrapper, element);

      // set element as child of wrapper
      wrapper.appendChild(element);

      wrapper.classList.add('ratio', 'ratio-16x9', 'mx-auto');
    }
  });
